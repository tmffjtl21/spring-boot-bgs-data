package bgs.tmffjtl.redis.account;

import javax.persistence.Id;

import org.springframework.data.redis.core.RedisHash;

import lombok.Data;

@Data
@RedisHash("accounts")
public class Account {
	
	@Id String id;
	private String username;
	private String password;
	private String email;
}
