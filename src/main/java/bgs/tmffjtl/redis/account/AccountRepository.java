package bgs.tmffjtl.redis.account;

import org.springframework.data.repository.CrudRepository;

// Spring 데이터의 최상위 repository 인터페이스  CrudRepository
// CrudRepository<Repository가 다루는 타입, 키의 타입> 
public interface AccountRepository extends CrudRepository<Account, String>{

}
