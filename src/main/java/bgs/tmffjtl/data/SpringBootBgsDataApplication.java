package bgs.tmffjtl.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBgsDataApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SpringBootBgsDataApplication.class);
		app.run(args);
	}
}
