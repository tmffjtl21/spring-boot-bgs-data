package bgs.tmffjtl.data;

import java.sql.Connection;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

//@Component
@Slf4j
public class MysqlRunner implements ApplicationRunner{

	@Autowired
	DataSource dataSource;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
//		
//		try(Connection connection = dataSource.getConnection()){
//			log.info(connection.getMetaData().getClass().getName());
//			log.info(connection.getMetaData().getURL());
//			log.info(connection.getMetaData().getUserName());
//			Statement statement = connection.createStatement();
//			String sql = "CREATE TABLE USER (ID INTEGER NOT NULL, name VARCHAR(255), PRIMARY KEY (id))";
//			statement.executeUpdate(sql);
//			connection.close();
//		}
//		
//		jdbcTemplate.execute("INSERT INTO USER VALUES (1, 'taejin')");
	}
}
