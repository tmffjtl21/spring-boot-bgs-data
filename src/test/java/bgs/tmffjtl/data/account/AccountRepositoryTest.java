package bgs.tmffjtl.data.account;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Optional;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import bgs.tmffjtl.data.account.AccountRepository;

@RunWith(SpringRunner.class)
@DataJpaTest	// Repository만 빈으로 등록하여 slicing test를 진행 , 임베디드 h2디비를 사용함 
//@SpringBootTest		// Integration test임 모든 어플리케이션의 어노테이션을 빈으로 등록하여 적용되어서 mysql을 사용함 하지만 임베디드 h2디비를 사용하는게 더 빠르고 좋음 
public class AccountRepositoryTest {
	@Autowired
	DataSource dataSource;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Test
	public void di() throws SQLException {
//		try(Connection connection = dataSource.getConnection()){
//			DatabaseMetaData metaData = connection.getMetaData();
//			System.out.println(metaData.getURL());
//			System.out.println(metaData.getDriverName());
//			System.out.println(metaData.getUserName());
//		}
		
		Account account = new Account();
		account.setUsername("taejin");
		account.setPassword("pass");
		
		Account newAccount = accountRepository.save(account);
		assertThat(newAccount).isNotNull();
		
		Optional<Account> existingAccount = accountRepository.findByUsername(newAccount.getUsername());
		assertThat(existingAccount).isNotEmpty();
		
		Optional<Account> nonExistingAccount = accountRepository.findByUsername("white");
		assertThat(nonExistingAccount).isEmpty();
		
	}
}